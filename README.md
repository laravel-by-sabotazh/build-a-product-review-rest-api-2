## Introduction

We will be building a product reviews API. Users will be able to add, view, update and delete products. Users will also be able to rate and review a product.

## Tutorial

From [here](https://dev.to/mr_steelze/build-a-product-review-rest-api-with-laravel-2ih8).

## Stack

Laravel Framework 10.x-dev

PHP 8.2.0

## License

Licensed under the [MIT license](https://opensource.org/licenses/MIT).
