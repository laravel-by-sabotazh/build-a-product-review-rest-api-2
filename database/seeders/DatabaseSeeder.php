<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\Review;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        User::factory(3)->create();

        Product::factory(5)
            ->has(Review::factory()->count(10))
            ->create();

//        $this->call([
//            UserSeeder::class,
//            Product::class,
//            Review::class
//        ]);
    }
}
